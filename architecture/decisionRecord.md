# Decision record  by Jose Castillo

This is a case study for the "Universidad Politécnica Salesiana" you can read about it at readme.md

# Architectural Solution for XYZ Company

## Status

This is the first version of an architecture proposed to provide a solution to the requirement of the XYZ company.
You can take a look at [Link text Here](https://1drv.ms/b/s!AlLdgVDf8TGaibsk2bIGpl9PvHAt8w?e=dVRaQX)

## Context

Basically the company needs to integrate its Odoo system with invoice providers, payment gateway and integration with social networks, facebook to advertise products and whatsapp to receive questions or possible sales

## Decision

1.  The architecture will be captured with C4 notation.
2.  All requirements must be enabled in the Microsoft cloud for free access by members of the development team, tests and stackholders.
3.  Azure as Infrastructure due to its security and multiple products that integrate with many technologies.
3.  A web application for e commerce will be created with Asp.net core.
4.  Minimal Net Core APIs are adopted for integration with Odoo APIs.
5.  Azure Storage Queue, service to store large amounts of messages and allows integration with multiple components.
6.  SignalR for direct communication with Twilio and manage WhatsApp message management
7.  Event Grid to work as a push mechanism, which means that the Event Grid pushes the event to its Subscribers.


## Consequences

Working with Microsoft technologies and an architecture based on services with queues and other APIs is ideal for integration with external partners.