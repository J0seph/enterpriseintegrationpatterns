workspace {
    model {
        customer = person "Customer" "A customer or potencial customer of XYZ Company." "Customer"
        
        enterprise "XYZ Company"{
            ecommerce     = softwareSystem "XYZ Solution" "Is a software with allows customer view and buy products"{
                webApplication = container "Ecommerce" "Display the products for sale ." "Asp.net core"
                webService = container "Web Service" "Provides services to comunicate with Odoo ERP" "Dotnet - Minimal API " "Web Browser"{
                    signalR = component "SignalR" "Modern websocket to set comunication between apps ." "Signal R"
                    odooController = component "API endpoints" "Sets comunication with Odoo ERP ." "API"
                }
                azureEventGrid = container "Azure Event Grid" "Provides a service to deliver message to subscriber destinations" "Azure platform" "Web Browser"
                iFrame = container "IFrame" "Provides a mechanism for payments" "ABC Payments" "Web Browser"
                
                microServices = container "Background Service For Facebook" "Take available products to post a facebook" "Dotnet - Asp.Net Core " "Backgorund service"{
                    azureQueue = component "Azure Queue Storage" "Products which will be published on Facebook." "Azure Queue Storage" "Queue"
                    consumer = component   "Consumer of message in order to post on facebook" "Dotnet - Asp.Net Core" "Backgound Service" 

                }
                
                
            }
            
            socialNetworks = softwareSystem "Social Networks" "Allow to advertise products." "Existing System"
            
            facebook    = softwareSystem "Facebook ADS system " "Allow to advertise products." "Existing System"
            whatsapp    = softwareSystem "Whatsapp enterprise" "Allow chat with users." "Existing System"
            payment     = softwareSystem "ABC Payments" "Allow to get payments of XYZ Company's customers." "Existing System"
            invoices    = softwareSystem "Invoice provider" "Host invoices for users." "Existing System"
            Odoo        = softwareSystem "Odoo ERP" "Accounting, manufacturing, warehouse, project management, and inventory management." "Existing System"
        }
        
        # relationships
        customer    ->  ecommerce   "View, search and buy products"
        ecommerce   ->  socialNetworks "Uses"

        ecommerce   ->  payment     "Allows to obtain the payment of the invoices"
        ecommerce   ->  invoices    "Allows to create and host invoices for users"
        ecommerce   ->  Odoo        "Transactions with inventories and sales"

        facebook    ->  customer    "Show new products"
        customer    ->  whatsapp    "Buy and ask info about products"
        
        # relationships to/from containers
        customer -> webApplication "Visits bigbank.com/ib using" "HTTPS"
        webApplication  ->  webService "Makes API calls to" "JSON/HTTPS"
        webApplication  ->  azureEventGrid "Makes API calls to" "JSON/HTTPS"
        webApplication  ->  iframe "Uses"
        azureEventGrid  ->  invoices "Delivers messages if receiver is available" "JSON/HTTPS"
        odooController  ->  Odoo "Makes API calls to" "JSON/HTTPS"
        signalR         ->  socialNetworks "Gets Whatsapp messages  API calls to" "JSON/HTTPS"
        
        iframe          ->  payment "Contains web component for payments"
        Odoo            ->  azureQueue "Products that will be published on Facebook"
        microServices    ->  socialNetworks "Post products on Facebook"
         
        
        # relationships to/from components
        consumer        ->  azureQueue "Consumes messages "
        signalR         ->  odooController "Uses "
    }
    
    
    
    views {
    
        
        systemContext ecommerce "SystemContext" {
            include *
            
             animation {
                customer 
                ecommerce
            }
            
            autoLayout
        }
        
        container ecommerce "Containers" {
            include *
            animation {
                customer 
                webApplication
            }
            autoLayout
        }
        
        component microServices "Components" {
            include *
            animation {
                azureQueue 
                consumer
            }
            autoLayout
        }
        
        component webService "ComponentsWebService" {
            include *
            animation {
                signalR 
                
            }
            autoLayout
        }
        
        styles {
            element "Person" {
                color #ffffff
                fontSize 22
                shape Person
            }
            element "Customer" {
                background #08427b
            }
            
            element "Software System" {
                background #1168bd
                color #ffffff
            }
            element "Existing System" {
                background #999999
                color #ffffff
            }
            element "Container" {
                background #438dd5
                color #ffffff
            }
            element "Web Browser" {
                shape WebBrowser
            }
            
            element "Queue" {
                shape Cylinder
            }
            element "Component" {
                background #85bbf0
                color #000000
            }
            element "Failover" {
                opacity 25
            }
        }
    }
}